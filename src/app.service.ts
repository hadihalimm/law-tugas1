import { ForbiddenException, Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { map, catchError } from 'rxjs';

@Injectable()
export class AppService {
  constructor(private http: HttpService) {}

  API_KEY = 'db7a2d8595654d00ac01cb312bcbec9d';

  async getAllIndonesiaStocks() {
    return this.http
      .get('https://api.twelvedata.com/stocks?country=Indonesia')
      .pipe(
        map((res) => {
          return res.data.data;
        }),
      )
      .pipe(
        catchError(() => {
          throw new ForbiddenException('Something went wrong.');
        }),
      );
  }

  async getLatestQuote(symbol: string) {
    console.log(symbol);
    return this.http
      .get(
        `https://api.twelvedata.com/quote?symbol=${symbol}&apikey=${this.API_KEY}`,
      )
      .pipe(
        map((res) => {
          console.log(res);
          return res.data;
        }),
      )
      .pipe(
        catchError(() => {
          throw new ForbiddenException('Something went wrong');
        }),
      );
  }

  async getPrice(symbol: string) {
    return this.http
      .get(
        `https://api.twelvedata.com/price?symbol=${symbol}&apikey=${this.API_KEY}`,
      )
      .pipe(
        map((res) => {
          return res.data;
        }),
      )
      .pipe(
        catchError(() => {
          throw new ForbiddenException('Something went wrong');
        }),
      );
  }
}
