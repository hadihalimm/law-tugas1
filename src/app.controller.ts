import { Controller, Get, Query } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  // @Get()
  // getHello(): string {
  //   return this.appService.getHello();
  // }

  @Get('indostocks')
  getAllIndonesiaStocks(): any {
    return this.appService.getAllIndonesiaStocks();
  }

  @Get('latest-quote')
  getLatestQuote(@Query('symbol') symbol: string): any {
    return this.appService.getLatestQuote(symbol);
  }

  @Get('get-current-price')
  comparePrice(@Query('symbol') symbol: string): any {
    return this.appService.getPrice(symbol);
  }
}
